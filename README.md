# Install

## Clone the repository
 
 ```
  git clone repository_URI
```



## Dependencies

Install dependencies
```
cd SQLAutomaticCorrector
virtualenv -p python3 python3env
source python3env/bin/activate
pip install flask
pip install flask-login
pip install flask-babel
pip install flask_sqlalchemy
pip install python3-saml # Para la autenticación con SAML2
pip install psycopg2    # Si la base de datos a sobre la que se quieren testetar consultas está en postgresql

pip install sqlparse
pip install re
```

Define the environment flask variables

```
export FLASK_APP=SQLAutomaticCorrector
export FLASK_ENV=development
```

## Configure the application

### Create and populate instance folder
  
```
mkdir instance
```
#### Create the config file

You can copy the config.sample.py file or create a new one
```
cp config.sample.py instance/config.py
```

TODO: Explain the different variables in the config file.

### Create the application database

#### Create the applicattion database admin user 
(it has to be able to create databases)

In sqlite3 it is not necessary

#### Create the application database files

```
flask initdb
```

#### Add an admin user to the database

This is usually the teacher(s) user

```
flask adduser --username admin  --name "Name of the admin user" --usertype admin
```

#### Add normal users to the database

These are usually the student users
``` 
flask adduser --username theusername  --name "Name of the user"
```


### Create the control database


#### Create the control databases admin user 
(it has to be able to create databases)

In postgresql 
```
CREATE ROLE sqladmin LOGIN CREATEDB NOCREATEROLE;
alter user sqladmin with password 'XXXXX';
```

#### Create the control databases normal user(s) 
(it has no admin permission)

In postgresql 
```
CREATE ROLE sqlcorrector LOGIN;
alter user sqlcorrector with password 'XXXXX';
```


#### Create the control database files

The config.py file has to have the users/passwords written

##### using the app orders

Use this way if the sql file has CREATE TABLE and INSERT orders. It does not function with COPY orders



```
flask initctrldb --dbid db1 --filename instance/SQLOrders.sql --admin
flask initctrldb --help
```


##### using psql

USE this way if the sql file has COPY orders (for example as a result of a default pg_dump order)

```
$ createdb db1
$ psql db1 < instance/SQLOrders.sql´´´
```


# Execute the application

Run the script run.sh

```
./run.sh
```