# This file has the configuration of the site.
# It has to be copied to the ''instance'' folder:
# cp config.sample.py instance/config.py

SECRET_KEY='theSecretKey'

# Application Database.
# servertype has to be sqlite3. TODO: add postgresql and may be others
DATABASE = {
        'servertype' : 'sqlite3',
        'host': None,
        'dbname': 'SQLAutomaticCorrector',
        'user': None,
        'password': None,
        'port': None
}

#User to create the applicattion database
DB_ADMIN_USER={
    'user':None,
    'password':None
}

# Languages in the applicattion
LANGUAGES={
            'en': 'English',
            'es': 'Spanish',
            'ca': 'Catalan'
}


# Users in this dictionary should only have permission to select in the database
# servertype has to be postgresql. TODO: add sqlite3 and may be other
# To create the user in postgresql:
# user=>  CREATE ROLE sqlcorrector LOGIN;
# user=> alter user sqlcorrector with password 'XXXXX';
CONTROL_DATABASES = {
    'db1' : {
        'servertype' : 'pg',
        'host': 'localhost',
        'dbname': 'dbname',
        'user': 'sqlcorrector',
        'password': 'thePass',
        'port': 3306
    },
    'db2' : {
        'servertype': 'pg',
        'host': 'localhost',
        'dbname': 'dbname2',
        'user': 'sqlcorrector',
        'password': 'thePass',
        'port': 3306
    }
}

#User to create the control databases. It has to have CREATEDB permission
# To create the user in postgresql:
# user=>  CREATE ROLE sqladmin LOGIN CREATEDB NOCREATEROLE;
# user=> alter user sqladmin with password 'XXXXX';
CTRLDB_ADMIN_USER = {
    'user':'sqladmin',
    'password':'theAdminPass'
}

# Admin email of the site
EMAIL_ADMIN="admin@example.com"

# SAML basic information
# If SAML is not used, define a void dictionary ({})
SAML_CONF = {
    'SAML_PATH':'samlfolder',     # Folder inside the instance folder to store the saml configuration
                                  # including the certs
    'SAML_IDP_NAME':'My favourite SSO'     # Name of the Idp
}

#SAML_CONF = {}

# Use only Saml to authenticat
ONLY_SAML = False    # True if only SAML authentication has to be used

#In expressions these chars are used to separate equivalent expressions (musthave and mustnot have expressions)
DELIMITERCHARS=",,,,"

LICENSE = {
           'url': 'https://gitlab.com/mperezfra/sqlautomaticcorrector/-/raw/master/LICENSE',
           'text': 'License'
}

SOURCE_CODE = {
           'url': 'https://gitlab.com/mperezfra/sqlautomaticcorrector/',
           'text': 'Source code'
}
