import functools

from flask import Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app

from werkzeug.security import check_password_hash, generate_password_hash
from werkzeug.urls import url_parse

from SQLAutomaticCorrector.models import *

# To avoid error: RuntimeError: Working outside of request context. FIXME
#from flask_babel import gettext
from gettext import gettext

from flask_login import UserMixin, login_required, current_user, login_user, logout_user

import click

class User(UserMixin):
    def __init__(self, userid):
        user = App_user.query.get(userid)

        self.id = user.userid
        self.username = user.username
        self.password = user.password
        self.usertype = user.usertype

    def get_user(username):
        user = App_user.query.filter(App_user.username==username).first()

        if user is None:
            return None

        return User(user.userid)

    def is_admin(self):

        if self.usertype == 'admin':  # FIXME
            return True
        return False

from . import login_manager

@login_manager.user_loader
def load_user(user_id):
    print(user_id)
    return User(user_id)

# url_prefix will be prepended to all the URLs associated with the blueprint bp
bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/login', methods=('GET', 'POST'))
def login():
    #Do not allow logged users to login again
    if current_user.is_authenticated:
        next = request.args.get('next')
        return redirect(next or current_app.config['APPLICATION_ROOT'])

    if current_app.config['ONLY_SAML']:
        return redirect("/saml2auth/?sso")


    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        #remember_me = request.form['remember_me']
        error = None

        if not username:
            error = gettext('Username required')
        elif not password:
            error = gettext('Pasword required')
        else:

            user=User.get_user(username)
            if user is None or not check_password_hash(user.password, password):
                error = gettext('Incorrect username and/or password.')

        if error is None:
            """session.clear()
            session['user_id'] = user['userid']
            session['user_type'] = user['usertype']"""
            login_user(user)

            next = request.args.get('next')
            if not next or \
               url_parse(next).netloc != '':  # the application only redirects when the URL is relative
                next = current_app.config['APPLICATION_ROOT']
            return redirect(next)

        flash(error)

    return render_template('auth/login.html')

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(current_app.config['APPLICATION_ROOT'])

# login_required is imported from flask_login
"""
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        # if there is no need to authenticate
        # if False and g.user is None:

        # and False: To allow everyone to access protected pages
        # if g.user is None and False:
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view
"""

def admin_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        # if there is no need to authenticate
        # if False and g.user is None:

        if current_user.is_anonymous:
            return redirect(url_for('auth.login'))
        elif not current_user.is_admin():
            return "No"   # FIXME
        return view(**kwargs)

    return wrapped_view


@bp.route('/users')
@bp.route('/<string:usertype>/users')
@admin_required
def users(usertype=None):
    """Show the users in the databsse"""

    if usertype:
        users = App_user.query.filter(App_user.usertype == usertype).all()

    else:
        users = App_user.query.all()

    return render_template("auth/users.html", users=users)


@bp.route('/register', methods=('GET', 'POST'))
@admin_required
def register():

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        name = request.form['name']
        usertype = request.form['usertype']

        error = None

        if not username:
            error = gettext('Username required')
        elif not password:
            error = gettext('Pasword required')
        elif not name:
            error = gettext('Name required')
        else:
            error = adduser(username, password, name, usertype)

        if error is None:
            return redirect(url_for("auth.register"))

        flash(error)

    return render_template('auth/register.html')

def adduser(username, password, name, usertype=""):

    error = None

    user = App_user.query.filter(App_user.username == username).first()

    if user is not None:
        error = gettext('User {} already registered'.format(username))

    if error is None:
        user = App_user(username=username, password=generate_password_hash(password), name=name, usertype=usertype)
        print("si")
        db.session.add(user)
        db.session.commit()

    return error




"""
@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    user_type = session.get('user_type')

    if user_id is None:
        g.user = None
    else:
<<<<<<< HEAD
        print("************")
        print(user_id)
        print(user_type)
        if False and user_type == "saml":   # FIXME: delete this saml users are included in the local database
            g.user={}
            g.user['user_id'] = user_id    
            g.user['user_type'] = user_type
        else:
            g.user = get_database().execute(
            'SELECT userid, username, password, usertype FROM user WHERE userid = ?', (user_id,)  ##### FIXME: password ????
            ).fetchone()
            print(g.user, g.user.keys())

@bp.route('/logout')
def logout():
    session.clear()
    return redirect("/")
    return redirect(url_for('index'))  # FIXME: Redirect to page caller

=======
        g.user = get_database().execute(
            'SELECT userid, username, password, usertype FROM user WHERE username = ?', (user_id,)
        ).fetchone()
        #print(g.user.keys())
"""

@click.command('adduser')
@click.option('--username', help="The user's username")
@click.option('--password', default="", help="The user's password. If it is not passed, it is asked", prompt=True, hide_input=True,
              confirmation_prompt=True)
@click.option('--name', help="The user's name" )
@click.option('--usertype', default="", help="It can be admin")
@with_appcontext
def add_user(username, password, name, usertype):
    """Adds a user to the table useres of the database

    flask adduser --username user1 --name theName

    flask adduser --username user1 --name theName --admin

    flask adduser --username user1 --name theName --admin --password myPass
    """

    error = adduser(username, password, name, usertype)

    if error is None:
        click.echo(gettext('User {} created.'.format(username)))
    else:
        click.echo(gettext('User {} not created.'.format(username)))
        click.echo(error)


def init_app(app):

    # adds adduser command that can be called with the flask command
    app.cli.add_command(add_user)






