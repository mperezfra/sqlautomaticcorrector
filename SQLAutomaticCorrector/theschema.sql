DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS question;
DROP TABLE IF EXISTS questionary;
DROP TABLE IF EXISTS questions_questionary;
DROP TABLE IF EXISTS answers_questionary;
DROP TABLE IF EXISTS answers_question;


CREATE TABLE user (
  userid INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  name TEXT NOT NULL,
  email TEXT UNIQUE NOT NULL,
  usertype TEXT
);

-- usertype  is student, teacher, admin

CREATE TABLE question (
  quenid INTEGER PRIMARY KEY AUTOINCREMENT,
  userid INTEGER NOT NULL,                    -- the author of the question
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  question TEXT NOT NULL,
  solution TEXT NOT NULL,
  FOREIGN KEY (userid) REFERENCES user (userid)
);

CREATE TABLE questionary (
  queyid INTEGER PRIMARY KEY AUTOINCREMENT,
  userid INTEGER NOT NULL,                    -- the author of the questionary
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  description TEXT NOT NULL,
  FOREIGN KEY (userid) REFERENCES user (userid)
);

CREATE TABLE questions_questionary (
  queyid INTEGER NOT NULL,
  quenid INTEGER NOT NULL,
  PRIMARY KEY (queyid,quenid),
  FOREIGN KEY (quenid) REFERENCES question,
  FOREIGN KEY (queyid) REFERENCES questionary
);









CREATE TABLE access_questionary (
  queyid INTEGER NOT NULL,
  userid INTEGER NOT NULL,
  timea TIMESTAMP NOT NULL,
  ipaddress INTEGER,
  PRIMARY KEY (queyid,userid, timea),
  FOREIGN KEY (queyid) REFERENCES questionary,
  FOREIGN KEY (userid) REFERENCES user
);

CREATE TABLE access_question (
  quenid INTEGER NOT NULL,
  userid INTEGER NOT NULL,
  timea TIMESTAMP NOT NULL,
  queyid INTEGER,
  ipaddress INTEGER,
  PRIMARY KEY (quenid,userid, timea),
  FOREIGN KEY (quenid) REFERENCES question,
  FOREIGN KEY (userid) REFERENCES user
);

CREATE TABLE answer_question (
  quenid INTEGER NOT NULL,
  userid INTEGER NOT NULL,
  timea TIMESTAMP NOT NULL,
  queyid INTEGER,
  ipaddress INTEGER,
  answer TEXT,
  queryCorrect INTEGER, -- 0 or 1
  PRIMARY KEY (quenid,userid, timea),
  FOREIGN KEY (quenid) REFERENCES question,
  FOREIGN KEY (userid) REFERENCES user
);
