# -*- coding: utf-8 -*-


# from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask.cli import with_appcontext
import click

#from flask_babel import gettext  # FIXME gives error

from gettext import gettext

db = SQLAlchemy()


# Stores the answer to a Question
class Answer_question(db.Model):
    __tablename__ = 'answer_question'
    quenid = db.Column(db.Integer, db.ForeignKey('question.quenid'), primary_key=True)
    userid = db.Column(db.Integer, db.ForeignKey('user.userid'), primary_key=True)
    timea = db.Column(db.DateTime, index=True, default=datetime.utcnow, nullable=False, primary_key=True)
    queyid = db.Column(db.Integer)
    ipaddress = db.Column(db.Integer)
    answer = db.Column(db.String)
    queryCorrect = db.Column(db.Integer)  # -- 0 or 1

    question = db.relationship("Question", back_populates="answers")
    user = db.relationship("App_user", back_populates="answers")


# Stores who access a Question
class Access_question(db.Model):
    __tablename__ = 'access_question'
    quenid = db.Column(db.Integer, db.ForeignKey('question.quenid'), primary_key=True)
    userid = db.Column(db.Integer, db.ForeignKey('user.userid'), primary_key=True)
    timea = db.Column(db.DateTime, index=True, default=datetime.utcnow, nullable=False, primary_key=True)
    queyid = db.Column(db.Integer)
    ipaddress = db.Column(db.Integer)

    question = db.relationship("Question", back_populates="accesses")
    user = db.relationship("App_user", back_populates="accesses")


class App_user(db.Model):
    __tablename__ = "user"
    userid = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, index=True, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    name = db.Column(db.String, nullable=False)
    email = db.Column(db.String(120), index=True, unique=True)
    usertype = db.Column(db.String)  # -- usertype  is student, teacher, admin

    questions = db.relationship('Question', backref='author', lazy=True)
    questionaries = db.relationship('Questionary', backref='author', lazy=True)

    answers = db.relationship('Answer_question', back_populates='user')
    accesses = db.relationship('Access_question', back_populates='user')

    accesses_qy = db.relationship('Access_questionary', back_populates='user')


class Question(db.Model):
    __tablename__ = "question"
    quenid = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    question = db.Column(db.String, nullable=False)
    solution = db.Column(db.String, nullable=False)
    # -- the author of the question
    userid = db.Column(db.Integer, db.ForeignKey("user.userid"), nullable=False)

    answers = db.relationship('Answer_question', back_populates='question')
    accesses = db.relationship('Access_question', back_populates='question')
    questionaries = db.relationship('Questions_questionary', back_populates='question')

    musthave = db.relationship('Musthave_expression', back_populates='question')
    mustnothave = db.relationship('Mustnothave_expression', back_populates='question')

# Stores the expression a Question must have
class Musthave_expression(db.Model):
    __tablename__ = 'musthave_expression'

    quenid = db.Column(db.Integer, db.ForeignKey('question.quenid'), primary_key=True)
    exprid = db.Column(db.Integer, primary_key=True)

    question = db.relationship("Question", back_populates="musthave")
    expression = db.Column(db.String, nullable=False)
    helpstring = db.Column(db.String, nullable=False)

# Stores the expression a Question must not have
class Mustnothave_expression(db.Model):
    __tablename__ = 'mustnothave_expression'

    quenid = db.Column(db.Integer, db.ForeignKey('question.quenid'), primary_key=True)
    exprid = db.Column(db.Integer, primary_key=True)

    question = db.relationship("Question", back_populates="mustnothave")
    expression = db.Column(db.String, nullable=False)
    helpstring = db.Column(db.String, nullable=False)


# Stores the questions in a Questionary
class Questions_questionary(db.Model):
    __tablename__ = 'questions_questionary'
    queyid = db.Column(db.Integer, db.ForeignKey('questionary.queyid'), primary_key=True)
    quenid = db.Column(db.Integer, db.ForeignKey('question.quenid'), primary_key=True)

    questionary = db.relationship("Questionary", back_populates="questions")
    question = db.relationship("Question", back_populates="questionaries")


# Stores who access a Questionary
class Access_questionary(db.Model):
    __tablename__ = 'access_questionary'
    queyid = db.Column(db.Integer, db.ForeignKey('questionary.queyid'), primary_key=True)
    userid = db.Column(db.Integer, db.ForeignKey('user.userid'), primary_key=True)
    timea = db.Column(db.DateTime, index=True, default=datetime.utcnow, nullable=False, primary_key=True)
    ipaddress = db.Column(db.Integer)

    questionary = db.relationship("Questionary", back_populates="accesses")
    user = db.relationship("App_user", back_populates="accesses_qy")


class Questionary(db.Model):
    __tablename__ = "questionary"
    queyid = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    description = db.Column(db.String, nullable=False)
    # -- the author of the questionary
    userid = db.Column(db.Integer, db.ForeignKey("user.userid"), nullable=False)

    accesses = db.relationship('Access_questionary', back_populates='questionary')  # Cascade ????? FIXME
    questions = db.relationship('Questions_questionary', back_populates='questionary')
    subjects = db.relationship('Questionaries_subject', back_populates='questionary')


class Subject(db.Model):
    __tablename__ = "subject"
    subjid = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String, nullable=False)
    name = db.Column(db.String, nullable=False)
    visible = db.Column(db.Boolean, nullable=False)
    academic_year = db.Column(db.String, nullable=False)

    __table_args__ = (
        db.UniqueConstraint('code', 'academic_year'),
    )

    questionaries = db.relationship('Questionaries_subject', back_populates='subject')


# Stores the questionaries in a Subject
class Questionaries_subject(db.Model):
    __tablename__ = 'questionaries_subject'
    queyid = db.Column(db.Integer, db.ForeignKey('questionary.queyid'), primary_key=True)
    subjid = db.Column(db.Integer, db.ForeignKey('subject.subjid'), primary_key=True)
    visible = db.Column(db.Boolean, nullable=False) # A questionary can be visible or not in a subject environment an visible or not in another subject

    questionary = db.relationship("Questionary", back_populates="subjects")
    subject = db.relationship("Subject", back_populates="questionaries")



def init_database():
    #FIXME: If the database already exists, what to do? remove or not?
    db.create_all()


@click.command('initdb')
@with_appcontext
def init_db_command():
    """Create the database for the app"""
    init_database()
    click.echo(gettext('Database initialized.'))


def init_app(app):
    # call close_database when cleaning up after returning the response
    # app.teardown_appcontext(close_database)
    # adds initdb command that can be called with the flask command
    app.cli.add_command(init_db_command)
