# -*- coding: utf-8 -*-

from flask import Blueprint, flash, g, redirect, render_template, request, url_for
from flask import current_app as this_app

from flask_login import current_user
from werkzeug.exceptions import abort

from SQLAutomaticCorrector.auth import login_required, admin_required
from SQLAutomaticCorrector.models import *
#from SQLAutomaticCorrector.controldatabase import get_database as get_ctrl_db

from gettext import gettext

from sqlalchemy import and_

#import psycopg2

from datetime import date, datetime

bp = Blueprint('subject', __name__, url_prefix='/subject')

@bp.route('/')
@login_required
def index():
    """ Show all the subjects"""

    subjects = (db.session.query(Subject.subjid, Subject.code, Subject.name, Subject.visible, Subject.academic_year)
                        .order_by(Subject.code)).all()

    return render_template('subject/index.html', subjects=subjects)

@bp.route('/create', methods=('GET', 'POST'))
#@bp.route('/create/questionay/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def create():
    """ Creates a new subject, """

    if request.method == 'POST':
        code = request.form['code']
        year = request.form['year']
        name = request.form['name']

        # print(request.form)

        # TODO: Test code does not exist already

        if request.form.get("visible"):
            visible = True
        else:
            visible = False

        error = None

        if not code:
            error = gettext('Code is required.')
        if not year:
            error = gettext('Academic year is required.')
        if not name:
            error = gettext('Name is required.')

        if error is not None:
            flash(error)
        else:

            subject = Subject(code=code, name=name, academic_year=year, visible=visible)

            db.session.add(subject)
            db.session.commit()

            return redirect(url_for('subject.index'))

    return render_template('subject/create.html')


###
### Functions to manage the relation between subject and questionaries
###
@bp.route('<int:subjid>/addquestionary/', methods=('GET', 'POST'))
@admin_required
def addQuestionary(subjid):
    """Add questionaries to a subject"""
    thisSubject = Subject.query.get(subjid)
    questionaries = (db.session.query(Questionary.queyid, Questionary.description, Questionaries_subject.visible)
                         .join(Questionaries_subject, Questionaries_subject.queyid == Questionary.queyid)
                         .join(Subject, Questionaries_subject.subjid == Subject.subjid)
                         .filter(and_(Questionaries_subject.subjid == subjid))).all()
    allQuestionaries = Questionary.query.all()

    if request.method == 'POST':
        selectectedQuestionaries = request.form.getlist('questionaries')

        if len(selectectedQuestionaries) > 0:
            for q in selectectedQuestionaries:
                questionaries_subject = Questionaries_subject(subjid=subjid, queyid=q, visible=True) # FIXME: read visible in the form ???

                #FIXME: Do not save the row if it already exists
                db.session.add(questionaries_subject)
            db.session.commit()

        return redirect(url_for('subject.update', subjid=subjid))

    return render_template('subject/addquestionary.html', subject=thisSubject, questionaries=allQuestionaries) # FIXME: pass only the questionaries not in the subject


@bp.route('<int:subjid>/removequestionary/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def removeQuestionary(subjid, queyid):
    """Remove a questionary from a subject"""

    #questionaries_subject = Questionaries_subject.query.get(subjid, queyid)
    questionaries_subject = Questionaries_subject.query.filter(and_(Questionaries_subject.subjid==subjid, Questionaries_subject.queyid==queyid)).first()
    db.session.delete(questionaries_subject)
    db.session.commit()

    return redirect(url_for('subject.update', subjid=subjid))


@bp.route('<int:subjid>/changevisibilityquestionary/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def changeVisibilityQuestionary(subjid, queyid):
    """Change the visibility of a questionary in a subject"""
    thisSubject = Subject.query.get(subjid)
    questionaries_subject = Questionaries_subject.query.filter(and_(Questionaries_subject.subjid==subjid, Questionaries_subject.queyid==queyid)).first()

    questionaries_subject.visible = not questionaries_subject.visible
    db.session.commit()

    return redirect(url_for('subject.update', subjid=subjid))


@bp.route('<int:subjid>/changevisibility', methods=('GET', 'POST'))
@admin_required
def changeVisibility(subjid):
    thisSubject = Subject.query.get(subjid)

    thisSubject.visible = not thisSubject.visible
    db.session.commit()

    return redirect(url_for('subject.index'))



#@bp.route('/update/<int:quenid>/questionary/<int:queyid>', methods=('GET', 'POST'))
@bp.route('<int:subjid>/update/', methods=('GET', 'POST'))
@admin_required
def update(subjid):
    """Updates the subject with id subjid,

    ???if the id of a questionary is given, it is associated to that questionary ???"""

    thisSubject = Subject.query.get(subjid)
    questionaries = (db.session.query(Questionary.queyid, Questionary.description, Questionaries_subject.visible)
                         .join(Questionaries_subject, Questionaries_subject.queyid == Questionary.queyid)
                         .join(Subject, Questionaries_subject.subjid == Subject.subjid)
                         .filter(and_(Questionaries_subject.subjid == subjid))).all()

    if request.method == 'POST':
        code = request.form['code']
        year = request.form['year']
        name = request.form['name']
        # TODO: Test code does not exist already

        if request.form.get("visible"):
            visible = True
        else:
            visible = False

        error = None

        if not code:
            error = gettext('Code is required.')
        if not year:
            error = gettext('Academic year is required.')
        if not name:
            error = gettext('Name is required.')

        if error is not None:
            flash(error)
        else:
            thisSubject.code = code
            thisSubject.academic_year = year
            thisSubject.name = name
            thisSubject.visible = visible
            db.session.commit()

            # if queyid:
            #     questions_questionary = Questions_questionary.query.filter(Questions_questionary.quenid == quenid, Questions_questionary.queyid == queyidold).first()
            #
            #     questions_questionary.queyid = queyid
            #     questions_questionary.quenid = quenid
            #
            #     db.session.add(questions_questionary)
            #     db.session.commit()

            # return redirect("/")
            return redirect(url_for('subject.index'))

    return render_template('subject/update.html', subject=thisSubject, questionaries=questionaries)


@bp.route('<int:subjid>/delete/', methods=('POST',))
@admin_required
def delete(subjid):
    """Deletes the subject with id subjid

    and the accesses to that question"""

    #FIXME: IF there are questionaries asigned to the subject, ask to delete them ????

    # Delete the subject
    subject = Subject.query.get(subjid)
    db.session.delete(subject)
    db.session.commit()

    return redirect(url_for('subject.index'))
