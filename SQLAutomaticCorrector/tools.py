#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
def new_object_id(test_list=None):
    # FIXME: Is there a better way to do this ?????
    MAXVAL = 500000000
    id = random.randint(1,MAXVAL)
    if test_list == None:
        return id
    else:
        # FIXME: Test if there is a new free value in test_list (between 1 and MAXVAL)
        while id in test_list:
            id = random.randint(1,MAXVAL)
        return id
