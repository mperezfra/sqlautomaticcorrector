# -*- coding: utf-8 -*-

from flask import Blueprint, flash, g, redirect, render_template, request, url_for, current_app
from flask import current_app as this_app

from flask_login import current_user
from werkzeug.exceptions import abort

from SQLAutomaticCorrector.auth import login_required, admin_required
from SQLAutomaticCorrector.models import *
from SQLAutomaticCorrector.controldatabase import get_database as get_ctrl_db
from SQLAutomaticCorrector.tools import new_object_id

from gettext import gettext

from sqlalchemy import and_

import psycopg2
import sqlparse
import re

from datetime import date, datetime

bp = Blueprint('question', __name__, url_prefix='/question')

@bp.route('/')
@login_required
def index():
    """ Show all the questions"""

    questions = (db.session.query(Question.quenid, Question.question, 
                                  Question.solution, Question.created, 
                                  App_user.userid, App_user.username)
                        .join(App_user, Question.userid == App_user.userid)
                        .order_by(Question.created)).all() 

    return render_template('question/index.html', questions=questions)

@bp.route('/create', methods=('GET', 'POST'))
@bp.route('/create/questionay/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def create(queyid=None):
    """ Creates a new question, if the id of a questionary is given, it is associated to that questionary """

    if request.method == 'POST':
        question = request.form['question']
        solution = request.form['solution']
        queyid = request.form['questionary']
        error = None

        if not question:
            error = gettext('Question is required.')
        if not solution:
            error = gettext('Solution is required.')

        #TODO: Test if the questionary exists in the database or is NULL

        if error is not None:
            flash(error)
        else:

            question = Question(question=question, solution=solution, userid=current_user.id)

            db.session.add(question)
            db.session.commit()

            if queyid:
                questions_questionary = Questions_questionary(queyid=queyid, quenid=question.quenid)

                db.session.add(questions_questionary)
                db.session.commit()

            return redirect(url_for('question.index'))

    return render_template('question/create.html', queyid=queyid)


###
### Functions to manage the relation between question and must have expressions
###
@bp.route('<int:quenid>/addmusthaveexpression/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def add_musthave_expression(quenid, queyid):
    """ Adds a 'must have' expression to a question """
    thisQuestion = Question.query.get(quenid)

    if request.method == 'POST':
        expression = request.form['expression']
        helpstring = request.form['helpstring']
        error = None

        if not expression:
            error = gettext('Expression is required.')
        if not helpstring:
            error = gettext('Help String is required.')

        if error is not None:
            flash(error)
        else:
            musthave_expression = Musthave_expression(quenid=quenid, exprid=new_object_id(), expression=expression, helpstring=helpstring)
            db.session.add(musthave_expression)
            db.session.commit()

        return redirect(url_for('question.update', quenid=quenid, queyid=queyid))

    return render_template('question/addmusthaveexpression.html', question=thisQuestion)

@bp.route('<int:quenid>/removemusthaveexpression/<int:exprid>/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def remove_musthave_expression(quenid, exprid, queyid):
    """ Removes a 'must have' expression from a question in a questionary"""

    musthave_expression = Musthave_expression.query.filter(and_(Musthave_expression.quenid==quenid, Musthave_expression.exprid==exprid)).first()
    db.session.delete(musthave_expression)
    db.session.commit()

    return redirect(url_for('question.update', quenid=quenid, queyid=queyid))

@bp.route('<int:quenid>/changemusthaveexpression/<int:exprid>/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def change_musthave_expression(quenid, exprid, queyid):
    """ Change the expression and help string of a 'must have' expression related to a question in a questionary"""
    thisQuestion = Question.query.get(quenid)
    expressionMustHave = Musthave_expression.query.filter_by(quenid=quenid, exprid=exprid).first()

    if request.method == 'POST':
        expression = request.form['expression']
        helpstring = request.form['helpstring']
        error = None

        if not expression:
            error = gettext('Expression is required.')
        if not helpstring:
            error = gettext('Help String is required.')

        if error is not None:
            flash(error)
        else:
            expressionMustHave.expression = expression
            expressionMustHave.helpstring = helpstring
            db.session.commit()

        return redirect(url_for('question.update', quenid=quenid, queyid=queyid))

    print(expressionMustHave)
    return render_template('question/updatemusthaveexpression.html', question=thisQuestion, expression=expressionMustHave)

###
### Functions to manage the relation between question and must not have expressions
###
@bp.route('<int:quenid>/addmustnothaveexpression/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def add_mustnothave_expression(quenid, queyid):
    """ Adds a 'must not have' expression to a question in a questionary"""
    thisQuestion = Question.query.get(quenid)

    if request.method == 'POST':
        expression = request.form['expression']
        helpstring = request.form['helpstring']
        error = None

        if not expression:
            error = gettext('Expression is required.')
        if not helpstring:
            error = gettext('Help String is required.')

        if error is not None:
            flash(error)
        else:
            mustnothave_expression = Mustnothave_expression(quenid=quenid, exprid=new_object_id(), expression=expression, helpstring=helpstring)
            db.session.add(mustnothave_expression)
            db.session.commit()

        return redirect(url_for('question.update', quenid=quenid, queyid=queyid))

    return render_template('question/addmustnothaveexpression.html', question=thisQuestion)

@bp.route('<int:quenid>/removemustnothaveexpression/<int:exprid>/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def remove_mustnothave_expression(quenid, exprid, queyid):
    """ Removes a 'must not have' expression from a question in a questionary"""

    mustnothave_expression = Mustnothave_expression.query.filter(and_(Mustnothave_expression.quenid==quenid, Mustnothave_expression.exprid==exprid)).first()
    db.session.delete(mustnothave_expression)
    db.session.commit()

    return redirect(url_for('question.update', quenid=quenid, queyid=queyid))

@bp.route('<int:quenid>/changemustnothaveexpression/<int:exprid>/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def change_mustnothave_expression(quenid, exprid, queyid):
    """ Change the expression and the help string of a 'must not have' expression related to a question in a questionary"""
    thisQuestion = Question.query.get(quenid)
    expressionMustNotHave = Mustnothave_expression.query.filter_by(quenid=quenid, exprid=exprid).first()

    if request.method == 'POST':
        expression = request.form['expression']
        helpstring = request.form['helpstring']
        error = None

        if not expression:
            error = gettext('Expression is required.')
        if not helpstring:
            error = gettext('Help String is required.')

        if error is not None:
            flash(error)
        else:
            expressionMustNotHave.expression = expression
            expressionMustNotHave.helpstring = helpstring
            db.session.commit()

        return redirect(url_for('question.update', quenid=quenid, queyid=queyid))

    return render_template('question/updatemustnothaveexpression.html', question=thisQuestion, expression=expressionMustNotHave)



@bp.route('/update/<int:quenid>/questionary/<int:queyid>', methods=('GET', 'POST'))
@bp.route('/update/<int:quenid>', methods=('GET', 'POST'))
@admin_required
def update(quenid, queyid=None):
    """Updates the question with id quenid, if the id of a questionary is given, it is associated to that questionary """

    thisQuestion = Question.query.get(quenid)

    expressionsMustHave = Musthave_expression.query.filter_by(quenid=quenid)
    expressionsMustnotHave = Mustnothave_expression.query.filter_by(quenid=quenid)

    if request.method == 'POST':
        question = request.form['question']
        solution = request.form['solution']
        queyid = request.form['questionary']
        queyidold = request.form['queidold']
        error = None

        if not question:
            error = gettext('Question is required.')
        if not solution:
            error = gettext('Solution is required.')

        #TODO: Test if the questionary exists in the database or is NULL

        if error is not None:
            flash(error)
        else:

            thisQuestion.question = question
            thisQuestion.solution = solution
            db.session.commit()

            if queyid:
                questions_questionary = Questions_questionary.query.filter(Questions_questionary.quenid == quenid, Questions_questionary.queyid == queyidold).first()

                questions_questionary.queyid = queyid
                questions_questionary.quenid = quenid

                db.session.add(questions_questionary)
                db.session.commit()

            return redirect(url_for('questionary.questionary', queyid=queyid))
            return redirect("/")
            return redirect(url_for('question.index'))

    return render_template('question/update.html', question=thisQuestion, queyid=queyid,
                           expressionsMustHave=expressionsMustHave, expressionsMustnotHave=expressionsMustnotHave)


@bp.route('/delete/<int:quenid>', methods=('POST',))
@admin_required
def delete(quenid):
    """Deletes the question with id quenid and the accesses to that question"""

    #FIXME: IF there are acceseses to the question, ask to delete them to or delete them directly ????
    # Delete all the accesses to this question
    accesses = Access_question.query.filter(Access_question.quenid == quenid).delete()

    #FIXME: IF there are answers to the question, ask to delete them to or delete them directly ????
    # ?????

    # Delete the questionary
    question = Question.query.get(quenid)
    db.session.delete(question)
    db.session.commit()

    return redirect(url_for('question.index'))

def check_queries(q1,q2, conn):
    """Executes the two queries over a connexion and test if the results are equal"""

    # Execute the first query
    cur1 = conn.cursor()
    try:
        cur1.execute(q1)
    except psycopg2.Error as e:
        print("I can't execute the first query!")
        print(q1)
        cur1.close()
        # Read the email administrator
        emailAdmin = this_app.config['EMAIL_ADMIN']
        return False, gettext("The control query can not be executed. Contact the admin ({})".format(emailAdmin))
        return render_template('question/answer_result.html', result=False, query=solution_given,
                               question=thisQuestion)
    rows1 = cur1.fetchall()
    cur1.close()

    # Execute the second query
    cur2 = conn.cursor()
    try:
        cur2.execute(q2)
    except psycopg2.Error as e:
        print("I can't execute the second query!")
        # FIXME: Informar de que hay un problema
        cur2.close()
        return False, gettext("Your query can not be executed")
        return render_template('question/answer.html', question=thisQuestion)
        # FIXME

    rows2 = cur2.fetchall()
    cur2.close()

    equalOutput = True
    # Test the number of rows
    if len(rows1) != len(rows2):
        # print("Diferentessss")
        if len(rows1)<len(rows2):
            errmes=gettext("Your query returns more rows than expected")
        else:
            errmes=gettext("Your query returns less rows than expected")
        return False, errmes
        return render_template('question/answer_result.html', result=False, query=solution_given,
                               question=thisQuestion)

    nrow=0
    for i in range(len(rows2)):
        # print("   ", rows2[i], " ", rows1[i])
        if rows1[i] != rows2[i]:
            errmes="In row {}, your query returns: <br /><br />".format(nrow)
            for r in rows2[i]:
                errmes += str(r) + ", "
            errmes += "<br /><br />and <br /><br />"
            for r in rows1[i]:
                errmes += str(r) + ", "
            errmes += "<br /><br />is expected.<br />"

            return False, errmes # FIXME

            break
        nrow += 1

    return True, "" # FIXME

def adjust_string(theString):
    #theString = theString.upper().replace(" ", "")
    # remove sqlcomments
    theString = sqlparse.format(theString, strip_comments=True)
    # remove all newlines, tabs and spaces.
    theString = re.sub("[\n\t\s]*", "",theString).upper()
    return theString

@bp.route('/answer/<int:quenid>/<int:queyid>', methods=('GET', 'POST'))
@bp.route('/answer/<int:quenid>', methods=('GET', 'POST'))
@login_required
def answer(quenid, queyid=None):

    thisQuestion = Question.query.get(quenid)

    # FIXME: Test if the questionary queyid has the question quenid ????

    if request.method == 'POST':   

        # Read the solution given to this question
        solution_given = request.form['answer']
        error = None
        # TODO: SQL INJECTION ??????
        #       Only one query, look for more than a ;

        ctrldbs = this_app.config['CONTROL_DATABASES']

        queryCorrect=True
        dbn=0
        for ctrldb in sorted(ctrldbs):
            dbn += 1;
            print(ctrldb)
            dbc = get_ctrl_db(ctrldb)
            if dbc is None:
                equals = False
                # Read the email administrator
                emailAdmin = this_app.config['EMAIL_ADMIN']
                error = gettext("The control database {} can not be opened. Contact the admin ({})".format(ctrldb, emailAdmin))
            else:
                equals, error = check_queries(thisQuestion.solution, solution_given, dbc)

            if not equals:
                queryCorrect=False
                break
            dbc.close()

        # The first database is the same that the users use to practice
        # if the error is detected in other control databases, a message is added
        if not queryCorrect and dbn > 1:
            mess = gettext("Your query returns the same rows in the practice database, <br /> but it is not correct as it returns diferent rows in other control databases.")
            error = mess + "<br />" + error

        # Check if the "Must have" expression are present in the solution and "Must not have" expressions are not
        musthave_expression = Musthave_expression.query.filter(Musthave_expression.quenid == quenid).all()

        delimchar = current_app.config['DELIMITERCHARS']
        errorMustHave = ""
        if musthave_expression:
            for e in musthave_expression:
                fmusthave = False
                for expres in e.expression.split(delimchar):
                    if re.search(adjust_string(expres),adjust_string(solution_given)):
                        fmusthave = True
                        break
                if not fmusthave:
                    errorMustHave += "<br />"+e.helpstring

        mustnothave_expression = Mustnothave_expression.query.filter(Mustnothave_expression.quenid == quenid).all()

        errorMustNotHave = ""
        if mustnothave_expression:
            for e in mustnothave_expression:
                fmustnohave = False
                for expres in e.expression.split(delimchar):
                    if re.search(adjust_string(expres),adjust_string(solution_given)):
                        fmustnohave = True
                        break
                if fmustnohave:
                    errorMustNotHave += "<br />" + e.helpstring

        if queryCorrect and (errorMustHave or errorMustNotHave):
            queryCorrect = False
            error+=gettext("Your query return the same rows as the correct solution but ...")
            error += errorMustHave
            error += errorMustNotHave
        # if the query is not correct the musthave and the mustnothave expressions are not shown
        # elif not queryCorrect:
            # error+=errorMustHave
            # error+=errorMustNotHave

        #Save time, user, IP and questionary    
        p = Answer_question(quenid = quenid, userid = current_user.id, 
                                timea = datetime.now(), queyid = queyid, ipaddress = request.remote_addr,
                                answer = solution_given, queryCorrect = queryCorrect)
        db.session.add(p)
        db.session.commit()

        return render_template('question/answer_result.html', result=queryCorrect, query=solution_given,
                                   question=thisQuestion, errorMessage=error, queyid=queyid )
    
    #Save time, user, IP and questionary
    p = Access_question(quenid=quenid, userid=current_user.id, 
                            timea=datetime.now(), queyid=queyid, ipaddress = request.remote_addr)
    db.session.add(p)
    db.session.commit()

    return render_template('question/answer.html', question=thisQuestion, queyid=queyid)
