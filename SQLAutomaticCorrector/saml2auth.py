
from flask import Blueprint, request, render_template, redirect, session, make_response, g, current_app
from flask_login import login_user, logout_user, current_user

from urllib.parse import urlparse

from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.utils import OneLogin_Saml2_Utils

from flask_babel import gettext

from SQLAutomaticCorrector.database import get_database
from SQLAutomaticCorrector.auth import User, adduser

# url_prefix will be prepended to all the URLs associated with the blueprint bp
bp = Blueprint('saml2auth', __name__, url_prefix='/saml2auth')


def init_saml_auth(req):
    s_auth = OneLogin_Saml2_Auth(req, custom_base_path=current_app.config['SAML_CONF']['SAML_PATH'])

    return s_auth


def prepare_flask_request(request):
    # If server is behind proxys or balancers use the HTTP_X_FORWARDED fields
    url_data = urlparse(request.url)
    return {
        'https': 'on' if request.scheme == 'https' else 'off',
        'http_host': request.host,
        'server_port': url_data.port,
        'script_name': request.path,
        'get_data': request.args.copy(),
        # Uncomment if using ADFS as IdP, https://github.com/onelogin/python-saml/pull/144
        # 'lowercase_urlencoding': True,
        'post_data': request.form.copy()
    }


@bp.route('/', methods=['GET', 'POST'])
def index():
    req = prepare_flask_request(request)
    s_auth = init_saml_auth(req)
    errors = []
    not_auth_warn = False
    success_slo = False
    attributes = False
    paint_logout = False

    # login process, send a AuthNRequestcall to the IdP, the call returns to ?acs
    if 'sso' in request.args:
        return redirect(s_auth.login())

    # login process, send a AuthNRequestcall to the IdP with a RelayState (return_to),
    # the call returns to ?acs
    elif 'sso2' in request.args:
        return_to = '%sattrs/' % request.host_url
        return redirect(s_auth.login(return_to))

    # Login process, a SAML Response has been arrived
    elif 'acs' in request.args:
        s_auth.process_response()
        errors = s_auth.get_errors()
        not_auth_warn = not s_auth.is_authenticated()
        print("autenticando")
        if len(errors) == 0:

            session['samlUserdata'] = s_auth.get_attributes()
            session['samlNameId'] = s_auth.get_nameid()
            session['samlSessionIndex'] = s_auth.get_session_index()
            
            user_name = session['samlUserdata']['uid'][0]
            print(user_name)
            #print(session['samlNameId'], session['samlSessionIndex'])
            
            local_user=User.get_user(user_name)
            
            # If the iser is not in the local database, add it as saml user
            if local_user is None:
                email = session['samlUserdata']['mail'][0]
                displayName = session['samlUserdata']['displayName'][0]
                # FIXME, use the User Class
                #db.execute('INSERT INTO  user(username, password, name, usertype) VALUES(?,?,?,?)', (user_name,'',displayName,'saml'))  #SAML users FIXME
                #db.commit() 

                error = adduser(user_name, '', displayName, 'saml')

                if error is not None:
                    flash(error)
                else:
                    local_user = User.get_user(user_name)
                    login_user(local_user)          
            else:	 
                login_user(local_user)          

            return redirect(current_app.config['APPLICATION_ROOT']) # FIXME or next ?

            self_url = OneLogin_Saml2_Utils.get_self_url(req)
            if 'RelayState' in request.form and self_url != request.form['RelayState']:
                return redirect(s_auth.redirect_to(request.form['RelayState']))

    # logout process, remove session an send a Logout Request to the IdP
    elif 'slo' in request.args:
        name_id = None
        session_index = None
        if 'samlNameId' in session:
            name_id = session['samlNameId']
        if 'samlSessionIndex' in session:
            session_index = session['samlSessionIndex']

        # FIXME
        # As some IdP do not return a Logout Response, clear the session and send a Logout Request
        logout_user()

        # https://github.com/onelogin/python-saml/issues/53#issuecomment-83653104
        # you may provide the same nameID and session_index that the IdP returned to the SP in the SAML Response:
        # https://github.com/onelogin/python-saml/blob/master/demo-flask/index.py#L58
        #print(name_id, session_index)

        #s_auth.logout(name_id=name_id, session_index=session_index)
        #return redirect(current_app.config['APPLICATION_ROOT'])
        return redirect(s_auth.logout(name_id=name_id, session_index=session_index))

    # logout process, a Logout Response has been arribed
    elif 'sls' in request.args:
        print("Logout Response executed FIXME *******************++++++++++++++++++")
        # return "Disconnected" 
        dscb = lambda: session.clear()
        return redirect(current_app.config['APPLICATION_ROOT']) # FIXME or next ?

        #FIXME: Eto de aquí bajo hay que ejecutarlo???
        url = s_auth.process_slo(delete_session_cb=dscb)
        errors = s_auth.get_errors()
        if len(errors) == 0:
            if url is not None:
                return redirect(url)
            else:
                success_slo = True

    if 'samlUserdata' in session:
        paint_logout = True
        if len(session['samlUserdata']) > 0:
            attributes = session['samlUserdata'].items()

    # FIXME: Show some information ???
    #return ""

    return render_template(
        'saml2auth/test.html',
        errors=errors,
        not_auth_warn=not_auth_warn,
        success_slo=success_slo,
        attributes=attributes,
        paint_logout=paint_logout
    )


@bp.route('/attrs/')
def attrs():
    paint_logout = False
    attributes = False

    if 'samlUserdata' in session:
        paint_logout = True
        if len(session['samlUserdata']) > 0:
            attributes = session['samlUserdata'].items()

    # FIXME: Show some information ???
    return ""
    return render_template('attrs.html', paint_logout=paint_logout,
                           attributes=attributes)


@bp.route('/metadata/')
def metadata():
    req = prepare_flask_request(request)
    s_auth = init_saml_auth(req)
    settings = s_auth.get_settings()
    metadata = settings.get_sp_metadata()
    errors = settings.validate_metadata(metadata)

    if len(errors) == 0:
        resp = make_response(metadata, 200)
        resp.headers['Content-Type'] = 'text/xml'
    else:
        resp = make_response(', '.join(errors), 500)
    return resp

