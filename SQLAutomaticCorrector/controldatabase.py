# -*- coding: utf-8 -*-
""" Module to manage the control databases, i.e, the database students use to practice and other databases with the same structure


"""

import psycopg2

from flask import g
from flask import current_app as this_app

from flask.cli import with_appcontext
import click

# from flask_babel import gettext  # FIXME gives error
from gettext import gettext

def get_database(dbid, admin=False):
    dbconf = this_app.config['CONTROL_DATABASES']
    if dbid not in dbconf.keys():
        print(gettext("Database {} not in control databases. Check config.py").format(dbid))
        return

    dbconf = dbconf[dbid]
    if admin:
        adminconf = this_app.config['CTRLDB_ADMIN_USER']
        db = "dbname='%s' user='%s' password='%s' host='%s' " % (dbconf['dbname'],
                                                             adminconf['user'],
                                                             adminconf['password'],
                                                             dbconf['host'])
    else:
        db = "dbname='%s' user='%s' password='%s' host='%s' " % (dbconf['dbname'],
                                                             dbconf['user'],
                                                             dbconf['password'],
                                                             dbconf['host'])


    if dbid not in g:
        # FIXME: put the previous code of this function inside this ???
        try:
            setattr(g,dbid,psycopg2.connect(db))
        except psycopg2.Error as e:
            print(gettext("Database not found, error: {}").format(e))

    return getattr(g, dbid, None)


def close_database(dbid):
    db = g.pop(dbid, None)

    if db is not None:
        db.close()
        
def init_database(dbid, filename, admin=False):
    """ Creates a database and execute the sql file containing the CREATE TABLES commands
        if everythig is correct it returns True, in other case it returns False

    """

    # Test if the database already exists
    db = get_database(dbid, admin)
    if db is not None:
        close_database(dbid)


    # Drop and create the database
    dbconf = this_app.config['CONTROL_DATABASES']
    if dbid not in dbconf.keys():
        print(gettext("Database {} not in control databases. Check config.py").format(dbid))
        return False
    dbconf = dbconf[dbid]
    if admin:
        adminconf = this_app.config['CTRLDB_ADMIN_USER']
        dbcs = "dbname='postgres' user='%s' password='%s' host='%s' " % (adminconf['user'],
                                                                         adminconf['password'],
                                                                         dbconf['host'])
        dbcs_newdb = "dbname='%s' user='%s' password='%s' host='%s' " % (dbconf['dbname'],
                                                                         adminconf['user'],
                                                                         adminconf['password'],
                                                                         dbconf['host'])

    else:
        dbcs = "dbname='postgres' user='%s' password='%s' host='%s' " % (dbconf['user'],
                                                                         dbconf['password'],
                                                                         dbconf['host'])
        dbcs_newdb = "dbname='%s' user='%s' password='%s' host='%s' " % (dbconf['dbname'],
                                                                         dbconf['user'],
                                                                         dbconf['password'],
                                                                         dbconf['host'])


    try:
        conn = psycopg2.connect(dbcs)
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute('DROP DATABASE IF EXISTS {};'.format(dbconf['dbname']))
        cur.execute('CREATE DATABASE {};'.format(dbconf['dbname']))
        conn.close()
        cur.close()

        db = get_database(dbid, admin)

    except psycopg2.Error as e:
        print(gettext("Database not created, error: {}").format(e))
        return False




    if db is not None:
        ok = execute_file_database(dbid, filename, admin)

        try:
            # Grant permissions to query the database to everybody
            cur = db.cursor()
            cur.execute('GRANT SELECT ON ALL TABLES IN SCHEMA public TO public;')
            # print(cur.query, cur.statusmessage)

            db.commit()

        except psycopg2.Error as e:
            print(gettext("Commands executed, but the permissions for database can not be changed, error: {}").format(e))
            return False

        close_database(dbid)

        return ok


def execute_file_database(dbid, filename, admin=False):
    """Executes a sql file in the database"""
    db = get_database(dbid, admin)

    if db is None:
        print(gettext("Database {} not found.").format(dbid))
        return False
    else:

        try:
            cur = db.cursor()
            # if the file is in the app folder
            # with this_app.open_resource(filename) as database:
            #    cur.execute(database.read().decode('utf8'))
            with open(filename, "r") as database:
                cur.execute(database.read())

            db.commit()

        except psycopg2.Error as e:
            print(gettext("Database Error: {}").format(e))
            return False
    return True

@click.command('initctrldb')
@click.option('--dbid', help="The id in the config file of the database to create")
@click.option('--filename', help="The filename containing the CREATE TABLES" )
@click.option('--admin/--no-admin', default=False, help="Execute the commands as a normal user or as an admin one")
@with_appcontext
def init_db_command(dbid, filename, admin):
    """Creates a control database

    flask initctrldb --dbid db1 --filename instance/dbcreate.sql

    flask initctrldb --dbid db1 --filename instance/dbcreate.sql --admin

    """
    if init_database(dbid, filename, admin):
        click.echo(gettext('{} control database initialized.'.format(dbid)))
    else:
        click.echo(gettext('{} control database not initialized.'.format(dbid)))


@click.command('execfilectrldb')
@click.option('--dbid', help="The id in the config file of the database to create")
@click.option('--filename', help="The filename containing the SQL orders to execute")
@click.option('--admin/--no-admin', default=False, help="Execute the commands as a normal user or as an admin one")
@with_appcontext
def exec_db_command(dbid, filename, admin):
    """Executes a sql file in a control database

    flask  execfilectrldb --dbid db1 --filename instance/dbdump.sql

    """
    if execute_file_database(dbid, filename, admin):
        click.echo(gettext('file {} executed in {} control database.'.format(filename, dbid)))
    else:
        click.echo(gettext('file {} not executed in {} control database.'.format(filename, dbid)))


#FIXME
#FIXME
#FIXME
#FIXME
def init_app(app):
    #  call close_database when cleaning up after returning the response
    app.teardown_appcontext(close_database)
    # adds initdb command that can be called with the flask command
    app.cli.add_command(init_db_command)
    app.cli.add_command(exec_db_command)





