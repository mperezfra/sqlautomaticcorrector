import os, sys

from flask import Flask, request, render_template
from flask_login import LoginManager

from flask_babel import Babel, gettext

login_manager = LoginManager()

#application factory function
def create_app(testconfig=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True) # configuration files are relative to the instance folder

    if testconfig is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config
        app.config.from_mapping(testconfig)

    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'

    babel = Babel (app)

    database = app.config['DATABASE']
    databaseType = database['servertype']

    if databaseType == 'sqlite3':
        app.config.from_mapping(
            SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(app.instance_path, database['dbname']+'.sqlite'),
            SQLALCHEMY_TRACK_MODIFICATIONS = False
        )
    # elif     # Other types of databases ...
    else:
        print(gettext("Database type not known {}").format(databaseType)) #FIXME
        sys.exit()

    # Create 'SAML_CONF' in app.config if it does not exists
    if 'SAML_CONF' not in app.config:
        app.config['SAML_CONF'] = {}

    # Test the existence of 'SAML_PATH' in app.config['SAML_CONF']
    if app.config['SAML_CONF'] != {}:
        if 'SAML_PATH' in app.config['SAML_CONF']:
            # set the absolute saml path
            app.config['SAML_CONF']['SAML_PATH'] = os.path.join(app.instance_path, app.config['SAML_CONF']['SAML_PATH'])
        else:
            print("To use SAML you have to define the saml folder")
            sys.exit()
    # does the instance folder exists?
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @babel.localeselector
    def get_locale():
        return request.accept_languages.best_match(app.config['LANGUAGES'].keys())

    from . import models
    models.db.init_app(app)
    models.init_app(app)

    """from . import database
    database.init_app(app)"""

    from . import controldatabase
    controldatabase.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)
    auth.init_app(app)

    from . import saml2auth
    app.register_blueprint(saml2auth.bp)

    from . import question
    app.register_blueprint(question.bp)

    from . import subject
    app.register_blueprint(subject.bp)

    from . import questionary
    app.register_blueprint(questionary.bp)

    # app.add_url_rule('/', endpoint='questionary.index')
    app.add_url_rule('/', endpoint='subject.index')

    return app
