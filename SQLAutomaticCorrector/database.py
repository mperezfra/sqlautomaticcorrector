# -*- coding: utf-8 -*-
""" Module to manage the app database: users, questions, questionaries, ...



"""

import sqlite3


from flask import g, current_app
from flask.cli import with_appcontext
import click

# from flask_babel import gettext  # FIXME gives error
from gettext import gettext

def get_database():
    if 'database' not in g:
        g.database = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.database.row_factory = sqlite3.Row

    return g.database


def close_database(ex=None):
    db = g.pop('database', None)

    if db is not None:
        db.close()
        
def init_database():
    db = get_database()

    with current_app.open_resource('theschema.sql') as database:
        db.executescript(database.read().decode('utf8'))



@click.command('initdb')
@with_appcontext
def init_db_command():
    """Create the database for the app"""
    init_database()
    click.echo(gettext('Database initialized.'))

def init_app(app):
    # call close_database when cleaning up after returning the response
    app.teardown_appcontext(close_database)
    # adds initdb command that can be called with the flask command
    app.cli.add_command(init_db_command)





