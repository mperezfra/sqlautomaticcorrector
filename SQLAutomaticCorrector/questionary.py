#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Blueprint, flash, g, redirect, render_template, request, url_for
from flask_login import current_user
from werkzeug.exceptions import abort

from SQLAutomaticCorrector.auth import login_required, admin_required
from SQLAutomaticCorrector.models import *

from sqlalchemy import and_

from gettext import gettext

from datetime import date, datetime

from SQLAutomaticCorrector.tools import new_object_id

bp = Blueprint('questionary', __name__, url_prefix='/questionary')


@bp.route('/')
@bp.route('/<int:subjid>', methods=('GET', 'POST'))
def index(subjid=None):
    """ Show all the questionaries, if a subject is passed, only the """

    if (subjid == None):
        questionaries = Questionary.query.all()
    else:
        questionaries = (db.session.query(Questionary.queyid, Questionary.description, Questionaries_subject.visible)
                         .join(Questionaries_subject, Questionaries_subject.queyid == Questionary.queyid)
                         .join(Subject, Questionaries_subject.subjid == Subject.subjid)
                         .filter(and_(Questionaries_subject.subjid == subjid))).all()
    return render_template('questionary/index.html', questionaries=questionaries)

@bp.route('/show/<int:queyid>', methods=('GET', 'POST'))
@login_required
def questionary(queyid):
    """ Show all the questions of the questionary"""

    questions = (db.session.query(Questionary.queyid, Questionary.description,
                                  Question.quenid, Question.question, Question.solution, Question.created, 
                                  App_user.userid, App_user.username, Questions_questionary)
                        .join(App_user, Question.userid == App_user.userid)
                        .join(Questions_questionary, Questions_questionary.quenid == Question.quenid)
                        .join(Questionary, Questions_questionary.queyid == Questionary.queyid)
                        .filter(and_(Questions_questionary.queyid == queyid))).all() 

    questionary = Questionary.query.get(queyid)
   
    #Save time, user, IP and questionary    
    p = Access_questionary(queyid=queyid, userid=current_user.id, 
                            timea=datetime.now(), ipaddress = request.remote_addr)
    db.session.add(p)
    db.session.commit()

    return render_template('questionary/questionary.html', questions=questions, questionary=questionary)

@bp.route('/create', methods=('GET', 'POST'))
@admin_required
def create():
    """Creates a new questionary """

    if request.method == 'POST':
        description = request.form['description']

        error = None

        if not description:
            error = gettext('Description is required.')

        if error is not None:
            flash(error)
        else:
            # Save the questionary with a random id
            questionaries_id = [r[0] for r in db.session.query(Questionary.queyid).all()]
            questionary = Questionary(queyid=new_object_id(questionaries_id), description=description, userid=current_user.id)

            db.session.add(questionary)
            db.session.commit()
            return redirect(url_for('questionary.index'))

    return render_template('questionary/create.html')

@bp.route('/update/<int:queyid>', methods=('GET', 'POST'))
@admin_required
def update(queyid):

    """Updates the description of the questionary queyid """
    questionary = Questionary.query.get(queyid)

    if request.method == 'POST':
        description = request.form['description']

        error = None

        if not description:
            error = gettext('Description is required.')

        if error is not None:
            flash(error)
        else:

            questionary.description = description;
            db.session.commit()

            return redirect(url_for('questionary.index'))

    return render_template('questionary/update.html', questionary=questionary)

# FIXME
#    if check_author and question['userid'] != g.user['userid']:
#        abort(403)

    #print(question)
    return questionary

@bp.route('/delete/<int:queyid>', methods=('POST',))
@admin_required
def delete(queyid):
    """Deletes the questionary with id queyid and the accesses to that questionary"""
    #FIXME: IF there are acceseses to the questionary, ask to delete them to or delete them directly ????

    # Delete all the accesses to this questionary
    accesses = Access_questionary.query.filter(Access_questionary.queyid == queyid).delete()

    # Delete the questionary
    questionary = Questionary.query.get(queyid)
    db.session.delete(questionary)
    db.session.commit()
    return redirect(url_for('questionary.index'))
